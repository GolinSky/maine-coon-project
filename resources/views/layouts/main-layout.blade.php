<!DOCTYPE html>
{{--<lang="{{ str_replace('_', '-', app()->getLocale()) }}">--}}
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Maine Coon</title>

    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <style>
        html
        {
            scroll-behavior: smooth;

        }

        #myBtn {
            display: none;
            position: fixed;
            bottom: 20px;
            right: 30px;
            z-index: 99;
            font-size: 18px;
            border: none;
            outline: none;
            background-color: red;
            color: white;
            cursor: pointer;
            padding: 15px;
            border-radius: 4px;
        }


        #myBtn:hover {
            background-color: #555;
        }

        #navbar {
            background-color: #333;
            position: fixed;
            top: 0px;
            width: 100%;
            display: block;
            transition: top 0.3s;
        }

        #navbar a.nav {
            float: right;
            display: block;
            color: #f2f2f2;
            text-align: center;
            padding: 15px 35px;
            text-decoration: none;
            font-size: 2vh;
        }

        #navbar a:hover {
            background-color: #ddd;
            color: black;
        }

        body .kitchen-content
        {
            padding-top: 30px;
        }
        html
        {
            background: #5a5d65;
            height: 100%;
            overflow-x: hidden;
            font-family: 'Nunito', sans-serif;

        }
        a:hover img
        {
            opacity: 100%;
            cursor: pointer;
        }
        .kitchen-content
        {
            text-align: center;
            justify-content: center;
            align-self: center;
            align-content: center;
            align-items: center;

        }
        .kitchen-content img
        {
            opacity: 80%;
            width: 50%;
            height: auto;
            text-align: center;
            vertical-align: top;

        }

        .kitchen-content h4
        {
            opacity: 100%;
            height: auto;
            text-align: center;



        }
        header.body-image
        {
            background-image: url("http://127.0.0.1:8000/images/test4.jpg");
            height: 100vh;
            width:100vw;

            /* Center and scale the image nicely */
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
            background-attachment: fixed;
            text-align: center;
            color: white;
            padding-top: 90vh;
            font-size: 4vh;
            display:block

        }
        footer
        {
            background: #0f1013;
            height: 10vh;

        }
        :root {
            --gutter: 20px;
        }

        .app {
            padding: var(--gutter) 0;
            display: grid;
            grid-gap: var(--gutter) 0;
            grid-template-columns: var(--gutter) 1fr var(--gutter);
            align-content: start;
        }

        .app > * {
            grid-column: 2 / -2;
        }

        .app > .full {
            grid-column: 1 / -1;
        }

        .hs {
            display: grid;
            grid-gap: calc(var(--gutter) / 2);
            grid-template-columns: 10px;
            grid-template-rows: minmax(150px, 1fr);
            grid-auto-flow: column;
            grid-auto-columns: calc(50% - var(--gutter) * 2);

            overflow-x: scroll;
            scroll-snap-type: x proximity;
            padding-bottom: calc(.75 * var(--gutter));
            margin-bottom: calc(-.25 * var(--gutter));
        }

        .hs:before,
        .hs:after {
            content: '';
            width: 10px;
        }


        * {
            box-sizing: border-box;
        }

        .container {
            position: relative;
        }

        .mySlides {
            display: none;
        }

        .cursor {
            cursor: pointer;
        }

        .prev,
        .next {
            cursor: pointer;
            position: absolute;
            top: 40%;
            width: auto;
            padding: 16px;
            margin-top: -50px;
            color: white;
            font-weight: bold;
            font-size: 20px;
            border-radius: 0 3px 3px 0;
            user-select: none;
            -webkit-user-select: none;
        }

        .next {
            right: 0;
            border-radius: 3px 0 0 3px;
        }

        .prev:hover,
        .next:hover {
            background-color: rgba(0, 0, 0, 0.8);
        }

        .numbertext {
            color: #f2f2f2;
            font-size: 12px;
            padding: 8px 12px;
            position: absolute;
            top: 0;
        }

        .caption-container {
            text-align: center;
            background-color: #222;
            padding: 2px 16px;
            color: white;
        }

        .row:after {
            content: "";
            display: table;
            clear: both;
        }

        .column {
            float: left;
            width: 33%;
        }

        .demo {
            opacity: 0.6;
        }

        .active,
        .demo:hover {
            opacity: 1;
        }
    </style>
    @yield(\App\Tools\ViewService::$Header)

</head>
<nav>
    <div id="navbar">
        @yield(\App\Tools\ViewService::$Nav)

    </div>
</nav>
<body>
<button onclick="topFunction()" id="myBtn" title="Go to top">Top</button>

@yield(\App\Tools\ViewService::$Body)

</body>

<footer>
    @yield(\App\Tools\ViewService::$Footer)
    <script>
        var prevScrollpos = window.pageYOffset;
        window.onscroll = function() {
            var currentScrollPos = window.pageYOffset;
            if (prevScrollpos > currentScrollPos) {
                document.getElementById("navbar").style.top = "0";
            } else {
                document.getElementById("navbar").style.top = "-150px";
            }
            prevScrollpos = currentScrollPos;
        }
    </script>

    <script>
        //Get the button
        var mybutton = document.getElementById("myBtn");

        // When the user scrolls down 20px from the top of the document, show the button
        window.onscroll = function() {scrollFunction()};

        function scrollFunction() {
            if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                mybutton.style.display = "block";
            } else {
                mybutton.style.display = "none";
            }
        }

        // When the user clicks on the button, scroll to the top of the document
        function topFunction() {
            document.body.scrollTop = 0;
            document.documentElement.scrollTop = 0;
        }
    </script>
</footer>
</html>
