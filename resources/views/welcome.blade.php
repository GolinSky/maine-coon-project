<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Maine Coon</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

        <!-- Styles -->
        <style>


            body .kitchen-content
            {
                padding-top: 30px;
            }
            html
            {
                background: #5a5d65;
                height: 100%;
                overflow-x: hidden;
                font-family: 'Nunito', sans-serif;

            }
            a:hover img
            {
                opacity: 100%;
                cursor: grab;
            }
            .kitchen-content img
            {
                opacity: 80%;
                width: 50%;
                height: auto;
                text-align: center;
                vertical-align: top;

            }
            header.body-image
            {
                background-image: url("test4.jpg");
                height: 100vh;
                width:100vw;

                /* Center and scale the image nicely */
                background-position: center;
                background-repeat: no-repeat;
                background-size: cover;
                background-attachment: fixed;
                text-align: center;
                color: white;
                padding-top: 90vh;
                font-size: 4vh;
                display:block

            }
            footer
            {
                background: #0f1013;
                height: 10vh;

            }
            :root {
                --gutter: 20px;
            }

            .app {
                padding: var(--gutter) 0;
                display: grid;
                grid-gap: var(--gutter) 0;
                grid-template-columns: var(--gutter) 1fr var(--gutter);
                align-content: start;
            }

            .app > * {
                grid-column: 2 / -2;
            }

            .app > .full {
                grid-column: 1 / -1;
            }

            .hs {
                display: grid;
                grid-gap: calc(var(--gutter) / 2);
                grid-template-columns: 10px;
                grid-template-rows: minmax(150px, 1fr);
                grid-auto-flow: column;
                grid-auto-columns: calc(50% - var(--gutter) * 2);

                overflow-x: scroll;
                scroll-snap-type: x proximity;
                padding-bottom: calc(.75 * var(--gutter));
                margin-bottom: calc(-.25 * var(--gutter));
            }

            .hs:before,
            .hs:after {
                content: '';
                width: 10px;
            }


            /* Demo styles */

            * {
                box-sizing: border-box;
            }

            /* Position the image container (needed to position the left and right arrows) */
            .container {
                position: relative;
            }

            /* Hide the images by default */
            .mySlides {
                display: none;
            }

            /* Add a pointer when hovering over the thumbnail images */
            .cursor {
                cursor: pointer;
            }

            /* Next & previous buttons */
            .prev,
            .next {
                cursor: pointer;
                position: absolute;
                top: 40%;
                width: auto;
                padding: 16px;
                margin-top: -50px;
                color: white;
                font-weight: bold;
                font-size: 20px;
                border-radius: 0 3px 3px 0;
                user-select: none;
                -webkit-user-select: none;
            }

            /* Position the "next button" to the right */
            .next {
                right: 0;
                border-radius: 3px 0 0 3px;
            }

            /* On hover, add a black background color with a little bit see-through */
            .prev:hover,
            .next:hover {
                background-color: rgba(0, 0, 0, 0.8);
            }

            /* Number text (1/3 etc) */
            .numbertext {
                color: #f2f2f2;
                font-size: 12px;
                padding: 8px 12px;
                position: absolute;
                top: 0;
            }

            /* Container for image text */
            .caption-container {
                text-align: center;
                background-color: #222;
                padding: 2px 16px;
                color: white;
            }

            .row:after {
                content: "";
                display: table;
                clear: both;
            }

            /* Six columns side by side */
            .column {
                float: left;
                width: 33%;
            }

            /* Add a transparency effect for thumnbail images */
            .demo {
                opacity: 0.6;
            }

            .active,
            .demo:hover {
                opacity: 1;
            }





        </style>
        <header class="body-image">Maine Coons Site</header>

    </head>

    <body>
        <div class="flex-center position-ref full-height" align="center">

            <h1>Kitchens</h1>
             <div class="kitchen-content">   <a><img src="test.jpg" >  <br>Kitchen 1</a></div>
             <div class="kitchen-content">   <a><img src="test.jpg" >  <br>Kitchen 1</a></div>
             <div class="kitchen-content">   <a><img src="test.jpg" >  <br>Kitchen 1</a></div>

            <br>
            <h1>  Champions</h1>


            <!-- Container for the image gallery -->
            <div class="container">

                <!-- Full-width images with number text -->
                <div class="mySlides">
                    <div class="numbertext">1 / 3</div>
                    <img src="test5.jpg" style="width:100%">
                </div>

                <div class="mySlides">
                    <div class="numbertext">2 / 3</div>
                    <img src="test2.jpg" style="width:100%">
                </div>
                <div class="mySlides">
                    <div class="numbertext">3 / 3</div>
                    <img src="test3.jpg" style="width:100%">
                </div>


                <!-- Next and previous buttons -->
                <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
                <a class="next" onclick="plusSlides(1)">&#10095;</a>

                <!-- Image text -->
                <div class="caption-container">
                    <p id="caption"></p>
                </div>

                <!-- Thumbnail images -->
                <div class="row">
                    <div class="column">
                        <img class="demo cursor" src="test5.jpg" style="width:100%" onclick="currentSlide(1)" alt="The Woods">
                    </div>
                    <div class="column">
                        <img class="demo cursor" src="test2.jpg" style="width:100%" onclick="currentSlide(2)" alt="The Woods">
                    </div>
                    <div class="column">
                        <img class="demo cursor" src="test3.jpg" style="width:100%" onclick="currentSlide(3)" alt="The Woods">
                    </div>
                </div>
            </div>

            <h1>  Contacts</h1>

        </div>

    </body>
<footer>
    <script>
        var slideIndex = 1;
        showSlides(slideIndex);

        // Next/previous controls
        function plusSlides(n) {
            showSlides(slideIndex += n);
        }

        // Thumbnail image controls
        function currentSlide(n) {
            showSlides(slideIndex = n);
        }

        function showSlides(n) {
            var i;
            var slides = document.getElementsByClassName("mySlides");
            var dots = document.getElementsByClassName("demo");
            var captionText = document.getElementById("caption");
            if (n > slides.length) {slideIndex = 1}
            if (n < 1) {slideIndex = slides.length}
            for (i = 0; i < slides.length; i++) {
                slides[i].style.display = "none";
            }
            for (i = 0; i < dots.length; i++) {
                dots[i].className = dots[i].className.replace(" active", "");
            }
            slides[slideIndex-1].style.display = "block";
            dots[slideIndex-1].className += " active";
            captionText.innerHTML = dots[slideIndex-1].alt;
        }
    </script>
</footer>
</html>

<div>{{--            @if (Route::has('login'))--}}
    {{--                <div class="top-right links">--}}
    {{--                    @auth--}}
    {{--                        <a href="{{ url('/home') }}">Home</a>--}}
    {{--                    @else--}}
    {{--                        <a href="{{ route('login') }}">Login</a>--}}

    {{--                        @if (Route::has('register'))--}}
    {{--                            <a href="{{ route('register') }}">Register</a>--}}
    {{--                        @endif--}}
    {{--                    @endauth--}}
    {{--                </div>--}}
    {{--            @endif--}}
</div>
