@extends('layouts.main-layout')

@section(\App\Tools\ViewService::$Header)
<header class="body-image">Mc Schatz</header>
<style>
    .tittle-block {
        position: absolute;
        bottom: 20px;
        background-color: black;
        color: white;
        padding-left: 20px;
        padding-right: 20px;
        right: 50%;
        transform: translate(50%, -50%);


    }
    .kitchen-content
    {
        position: relative;
    }
    .tittle-text
    {
        text-align: center;
    }





</style>
@endsection

@section(\App\Tools\ViewService::$Nav)
    <a class = "nav" href="#litters">Litters</a>
    <a class = "nav" href="#champions">Champions</a>
    <a class = "nav" href="#contact">Contact</a>
@endsection

@section(\App\Tools\ViewService::$Body)

<div class="flex-center position-ref full-height" align="center">
    <h1 id = "litters">Kitchens</h1>

    @forelse($litters as $litter)
        <div class="kitchen-content">
            <a href="{{$litter->ShowRoute()}}">
                <img src="{{$litter->image_path}}" >
            </a>
            <div class ="tittle-block">
                <h4 class = "tittle-text">{{$litter->name}}</h4>
            </div>

        </div>
    @empty
        <p>No litters yet</p>
    @endforelse


    <br>
    <h1 id = "champions">  Champions</h1>
    @forelse($cats as $cat)
        <div class="kitchen-content">   <a href="{{$cat->ShowRoute()}}"><img src="{{$cat->image_path}}" >  <br>{{$cat->name}}</a></div>
    @empty
        <p>No Champions yet</p>
    @endforelse
    <h1 id = "contact">  Contacts</h1>

</div>
@endsection


