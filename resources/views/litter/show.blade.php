@extends('layouts.main-layout')


@section(\App\Tools\ViewService::$Header)
<header class="body-image">{{$litter->name}}</header>
    @endsection


@section(\App\Tools\ViewService::$Nav)
    <a class = "nav"  href="#kittens">Kittens</a>
    <a class = "nav" href="{{route('litter.index')}}">Home</a>

@endsection

@section(\App\Tools\ViewService::$Body)

    <div class="flex-center position-ref full-height" align="center">
        <h1 id ="kittens">Kittens</h1>


            @forelse($kittens as $kitten)
                <div class="kitchen-content">   <a href="{{$kitten->ShowRoute()}}"><img src="{{asset($kitten->image_path) }}" >  <br>{{$kitten->name}}</a></div>
            @empty
                <p>No Kittens yet</p>
            @endforelse
    </div>
@endsection
