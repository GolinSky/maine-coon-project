<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Litter extends Model
{
    //
    public static function Names() :array
    {
        $name = DB::table('litters')->select('name')->pluck('name');

       return $name->toArray();
    }

    public static function Ids() :array
    {
        $name = DB::table('litters')->select('id')->pluck('id');

        return $name->toArray();
    }

    public function ShowRoute()
    {
        return route('litter.show', $this);
    }

}
