<?php

namespace App\Http\Controllers;

use App\Kitten;
use Illuminate\Http\Request;

class KittenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Kitten  $kitten
     * @return \Illuminate\Http\Response
     */
    public function show(Kitten $kitten)
    {
        return view('kitten.show', [
            'kitten' => $kitten
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Kitten  $kitten
     * @return \Illuminate\Http\Response
     */
    public function edit(Kitten $kitten)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Kitten  $kitten
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Kitten $kitten)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Kitten  $kitten
     * @return \Illuminate\Http\Response
     */
    public function destroy(Kitten $kitten)
    {
        //
    }
}
