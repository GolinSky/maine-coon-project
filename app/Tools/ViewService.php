<?php


namespace App\Tools;


class ViewService
{
    public static string $Header  = "header";
    public static string $Body  = "body";
    public static string $Nav  = "navigation";
    public static string $Footer  = "footer";
}
