<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cat extends Model
{
    //

    public function ShowRoute()
    {
        return  route('cat.show', $this);
    }
}
