<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kitten extends Model
{
    //

    public function ShowRoute()
    {
        return  route('kitten.show', $this);
    }
}
